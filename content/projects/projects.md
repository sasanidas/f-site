+++
title = "Projects"
path = "projects"
+++

***

## [CLEDE](https://gitlab.com/sasanidas/clede)

Common Lisp Emacs Development Environment (CLEDE) is a collection of utilities and tools for a better CL Emacs experience.

***

## [maxima.el](https://gitlab.com/sasanidas/maxima)

Emacs interface for [maxima](https://maxima.sourceforge.io/), a CAS(Computer Algebra System) system.

***

## [simple-tree](https://gitlab.com/sasanidas/simple-tree.el)

A simple tree project organizer.

***

## [realgud-xdebug](https://github.com/realgud/realgud-xdebug)

Emacs state of the art, debugger interface ([Realgud](https://github.com/realgud/realgud-xdebug)), for [xdebug](https://xdebug.org/).

***

## [lsp-sonarlint](https://github.com/emacs-lsp/lsp-sonarlint)

An lsp client implementation of the linter tool from [sonarsource](https://www.sonarsource.com/) for Emacs.

***

## [Commodore 64 IDE](https://gitlab.com/sasanidas/emacs-c64-basic-ide)

Emacs package for development in Commodore BASIC, with autocompletion and on fly evaluation and syntax checking

***

## [company-picoLisp](https://gitlab.com/sasanidas/company-plisp)

Emacs company module for autocompletion in the PicoLisp programming language.

***

## [lsp-serenata](https://emacs-lsp.github.io/lsp-mode/page/lsp-serenata/)

Contribution to the main emacs package [lsp-mode](https://github.com/emacs-lsp/lsp-mode/) of the PHP lsp server.

***

#### More on my gitlab [here](https://gitlab.com/users/sasanidas/projects) and my [github](https://github.com/sasanidas)




