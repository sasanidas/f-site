+++
title = "Explorando Emacs [1 parte]"
description = "Article"
+++

- [Explorando Emacs [1 parte]](#org05e8a6b)
  - [zone.el](#org64720c1)
  - [cedet](#orged750e0)
    - [semantic](#orgb078959)
    - [speedbar](#org37478cc)
  - [woman.el](#org6fb4aee)
  - [hideshow.el](#orgfea9947)


<a id="org05e8a6b"></a>

# Explorando Emacs [1 parte]

En un inicio podríamos estar tentados en la idea mas o menos moderna de instalar plugins desde el repositorio por excelencia, el lugar donde mas paquetes tenemos a nuestro alcance. Es una aproximación mas o menos razonable, Emacs en primera instancia, parece algo bastante, "primitivo", para los estándares actuales (entendiendo estándares como los editores mas populares en 2020), pero lo que muchos usuarios relativamente nuevos no conocen, son las herramientas que en Emacs vienen por defecto. Bien, me he dado una vuelta por los mas desconocidos de Emacs para traeros esta pequeña lista de aquellos que me han parecido mas interesantes:


<a id="org64720c1"></a>

## zone.el

Dentro de la carpeta lisp/play, podemos encontrar todo tipo de juegos y hacks interesantes. Ciertamente no me atrae Emacs por su capacidad como consola, con lo que la gran mayoría me son indiferentes (mas allá de enseñarlos como curiosidad).

Este archivo es algo peculiar, es algo así como un "salva pantallas", que puedes programar para que pasado X tiempo, haga todo tipo de cambios visuales en el buffer actual. Para probarlo, simplemente es ejecutar el comando `zone`, y veremos como nuestro buffer se vuelve algo loco.

Por si mismo, esto es una gracieta sin mas, pero a mi me resulta mas interesante esta configuración:

```lisp

(use-package zone)
(zone-when-idle 180)

```

Este código activara zone, cuando estemos 3 minutos sin hacer nada.


<a id="orged750e0"></a>

## cedet

Puede que para muchos usuarios mas veteranos de Emacs, este paquete sea algo mas que obvio, pero creo (con relativa certeza),que la gran mayoría de usuarios van directos a MELPA->lsp-mode->ccls, sin pararse a preguntar si Emacs tiene algo que ofrecer para el desarrollo en C/C++. Respecto al desarrollo en C/C++, ya hice una revisión interesante hace algún tiempo (puede verse [aqui](https://sasanidas.gitlab.io/f-site/cpp-development/)), utilizando un mix de utilidades, combinando las que Emacs nos ofrece y alguna interesante de MELPA.

Pero CEDET es algo diferente en ese aspecto, es un conjunto de utilidades a cual mas interesante, os invito ciertamente a pasaros por la [web principal](http://cedet.sourceforge.net/) de CEDET para explorar a fondo sus características, pero mientras, aqui os traigo algunas de las herramientas que mas me han llamado la atención:


<a id="orgb078959"></a>

### semantic

Podríamos definir semantic como el conjunto de utilidades que posee emacs para parsear y "entender" el código fuente de diferentes lenguajes, teniendo en su caja de herramientas dos analizadores lexicos y dos generadores de parser.

Esto escrito en elisp e interconectado con otras geniales herramientas también incluidas en Emacs como speedbar o imenu.

Para utilizar semantic en C/C++ podemos añadir esta configuración:

```lisp
(add-hook 'c++-mode-hook #'semantic-mode)
(add-hook 'c-mode-hook #'semantic-mode)
```

Esto activara semantic al iniciar los modos c++-mode y c-mode, también nativos de Emacs.


<a id="org37478cc"></a>

### speedbar

Speedbar es otro de esos paquetes que pasan fácilmente desapercibidos, pero que es bastante intuitivo y útil. Este programa es el encargado de generar una pequeña ventana al lado de nuestro buffer, el cual nos puede dar diferentes tipos de información, desde un navegador de archivos del proyecto hasta la informacion semantica de nuestro actual "scope". Esta opción es la mas interesante cuando desarrollo C/C++, siendo tan sencillo de utiliar como llamar a la función `semantic-speedbar-analysis` (teniendo previamente activado semantic).

Nos dara informacion actualizada de la funcion donde nos encontramos, y podremos navegar facilmente entre sus links interactivos.


<a id="org6fb4aee"></a>

## woman.el

Si estabas utilizando eshell junto con man para mirar los manuales de tu sistema GNU, esta alternativa te resultara de lo mas interesante. En esencia es un navegador de las paginas man del sistema y&#x2026; ya esta, es básicamente eso, sencillo de usar y sin ninguna dependencia, puede ser utilizado llamando a `woman`.


<a id="orgfea9947"></a>

## hideshow.el

Muchos editores modernos presumen de la opción de contraer y expandir las funciones de forma dinámica, Emacs por supuesto no iba a ser menos. Este pequeño paquete nos permite precisamente eso, y para activarlo es tan sencillo como llamar a `hs-minor-mode` en el buffer donde queramos usarlo. También de forma mas conveniente podríamos configurarlo como un hook. En definitiva, un paquete sencillo y útil que viene en Emacs por defecto, para deleite de todos.
