+++
title = "Emacs development in C/C++"
description = "Article"
+++

- [Emacs development in C/C++](#org5e4b6fd)
  - [Basic elements](#org3a346da)
    - [Formatting and indentation](#orgb03e359)
    - [Auto-complete and function information](#orged36957)
    - [Error checking](#orgac6e791)
    - [Parse utilities](#org24fc68f)
    - [Indexing and source code browsing](#org127197f)
    - [Debug](#org37d0408)
  - [More features](#orgd3269d0)
    - [Refactoring](#org9d2814f)
    - [Cmake](#orgc1b6c29)
    - [Emacs tree-sitter](#org2396bd8)
  - [Extra features](#orga1a0ba4)
    - [Hydra](#org75f91de)
    - [Skeletor](#org71f030d)
  - [Alternatives](#orgc4771e1)
    - [LSP](#orgdf71783)
    - [Rtags](#orgd5dc9b9)
  - [Try it!](#orgd9d8e77)
  - [Final thoughts](#orgf8f1831)


<a id="org5e4b6fd"></a>

# Emacs development in C/C++

Historically Emacs always had a great C/C++ development environment, but all these years a lot of others IDEs and editor are catching up with features for today C/C++ development.

This article aims to create a simple guide to add all the features for a complete C/C++ development environment inside Emacs.

This article also assume that some tools are already installed in Emacs, such as `projectile` , `use-package` and some short of `helm` / `ivy`.


<a id="org3a346da"></a>

## Basic elements


<a id="orgb03e359"></a>

### Formatting and indentation

-   [clang-format](https://melpa.org/#/clang-format)

Formatting and indentation is vital in all serious development environments, for emacs there is a couple of interesting options, but for me, the most easiest one is \`clang-format\`, and easy OOTB format tool.

```lisp

	 (defun llvm-lineup-statement (langelem)
	   (let ((in-assign (c-lineup-assignments langelem)))
	     (if (not in-assign)
		 '++
	       (aset in-assign 0
		     (+ (aref in-assign 0)
			(* 2 c-basic-offset)))
	       in-assign)))

	 ;; Add a cc-mode style for editing LLVM C and C++ code
	 (c-add-style "llvm"
		      '("gnu"
			(fill-column . 80)
			(c++-indent-level . 2)
			(c-basic-offset . 2)
			(indent-tabs-mode . nil)
			(c-offsets-alist . ((arglist-intro . ++)
					    (innamespace . 0)
					    (member-init-intro . ++)
					    (statement-cont . llvm-lineup-statement)))))
	(use-package clang-format
	  :ensure t
	  :config
	  (let* ((style "llvm"))
	    (setq clang-format-style (upcase style))
	    (c-set-style style)))


```

As we can see, we can specify the code style for clang-format and c-default-style at the same time, it is not mandatory, but in some cases convenient.

The Emacs command `clang-format` will format our code according with the style defined.


<a id="orged36957"></a>

### Auto-complete and function information

-   [company](https://melpa.org/#/company)
-   [irony](https://melpa.org/#/irony)
-   [company-irony](https://melpa.org/#/company-irony)
-   [irony-eldoc](https://melpa.org/#/irony-eldoc)
-   [company-c-headers](https://melpa.org/#/company-c-headers)

Auto-complete is also a core feature for the desired development environment, in this regards, Emacs also have a variety of package to chose from, but just for simplicity, we are going to use a couple of small one, instead of a more extensive one like [rtags](https://github.com/Andersbakken/rtags), don't get me wrong, I think that rtags it's a great package, but a little complex for my taste.

For the core auto-complete, we are going to use primary `irony`, a completion server base on [libclang](https://clang.llvm.org/doxygen/group__CINDEX.html), that in my opinion brings great results and in the top of that is easy to install and use. Irony also have a company package called `company-irony`, that easily integrate company with irony, transparent for the end user.

There is only one small detail, at least for me, irony doesn't complete C/C++ headers, so for that task, we are going to use `company-c-headers`, which are also valid for C++ headers.

We can also use irony with `eldoc` for function information.

```lisp
(use-package irony
  :ensure t
  :config (irony-mode))

(use-package company-irony
  :ensure t
  :after (irony company)
  :config (add-to-list 'company-backends 'company-irony))


(use-package irony-eldoc
  :ensure t
  :after irony
  :init (irony-eldoc))

(use-package company-c-headers
  :ensure t
  :config
  (add-to-list 'company-c-headers-path-system "/usr/include/c++/8/")
  (add-to-list 'company-backends 'company-c-headers))

```

For the C++ headers auto-complete, it is necessary to specify the C++ headers path in the system.


<a id="orgac6e791"></a>

### Error checking

-   [flycheck](https://melpa.org/#/flycheck)

On fly error checking is also a great feature for any programming environment, Emacs has by default `flymake` , but I strongly prefer `flycheck` for that task, is easy to use and has more features (for comparison [here](https://www.flycheck.org/en/latest/user/flycheck-versus-flymake.html)).

```lisp

(use-package flycheck
  :ensure t
  :init (flycheck-mode))

```

By default it uses the clang checker, awesome for C/C++.


<a id="org24fc68f"></a>

### Parse utilities

-   [semantic](https://www.gnu.org/software/emacs/manual/html_node/semantic/index.html#Top)

Another great features that some people think is unique of IDEs is the ability to parse and "understand" the source code, that's far from reality, Emacs even have a complete framework for parsing and analyzing the source code, and fortunately for us, it supports both C and C++.

Easy to use and with some interesting command, it can also provide some completion and references among other things.

```lisp

(semantic-mode)

```

Once the source code has been parse, we can start using some useful functions.

| Function                             | Explanation                                  |
|------------------------------------ |-------------------------------------------- |
| (if helm is installed) helm-semantic | Show semantic units in the current file      |
| semantic-symref                      | Show reference of the selected semantic unit |
| semantic-ia-fast-jump                | Go to the definition of the semantic unit    |
| semantic-ia-show-doc                 | Show the documentation of the selected unit  |


<a id="org127197f"></a>

### Indexing and source code browsing

-   [universal-ctags](https://ctags.io/)

There are a couple of ways to provide code navigation, but in general, tagging still in my opinion is a decent way to browse project units.For that task, there are a couple of backends like [gnu-global](https://www.gnu.org/software/global/),[exuberant-tags](http://ctags.sourceforge.net/) and universal-ctags, the last one is the most interesting in my opinion, is has been recently updated and recently a new C++ parser has been add, making this tool ideal for our configuration (C++ parse reference: [universal-ctags.pdf](https://readthedocs.org/projects/ctags/downloads/pdf/latest/) page 36).

The tool can be invoke from the command line, but we are in Emacs, so we can easily create a simple function to create and update our TAG file.

```lisp

(defun user/generate-tags ()
  "Generate TAG file for C/C++ projects"
  (interactive)
  (let* ((project-root (projectile-project-root)))
    (shell-command
     (concat "cd " 
	     (shell-quote-argument project-root)  
	     " && ctags-universal -h \".h.hpp.cpp.c\" -e -R " (shell-quote-argument project-root)))))


```

This function override the entire TAG files every time is invoke, for a more efficient approach, you can look up &#x2013;append option, but for small/medium project, I didn't fine any performance issues.

Once we have our project indexed, we can use some useful `xref` features, a built in package.

| Function                                | Explanation                                  |
|--------------------------------------- |-------------------------------------------- |
| xref-find-references                    | Show references of the selected tag element  |
| xref-find-definitions                   | Show definitions of the selected tag element |
| (if helm is installed)helm-etags-select | Navigate through project tags                |

For more information about xref: [xref](https://www.gnu.org/software/emacs/manual/html_node/emacs/Xref.html)


<a id="org37d0408"></a>

### Debug

-   [gdb](https://sourceware.org/gdb/current/onlinedocs/gdb/)
-   [emacs-gdb](https://www.gnu.org/software/emacs/manual/html_node/emacs/GDB-Graphical-Interface.html)

Emacs provide excellent support for the `gdb` debugger, so there is no need for external packages in this regard.

The gdb documentation is great an extensive, I really recommend to check it out, also this is a small code for some visual information with gdb.

```lisp

(setq
 ;; use gdb-many-windows by default
 gdb-many-windows t
 ;; Non-nil means display source file containing the main routine at startup
 gdb-show-main t)

```


<a id="orgd3269d0"></a>

## More features

These features are an interesting addition for our Emacs C/C++ environment, and for some people it may be mandatory, so if you are interesting to improve and extend your programming experience, I really encourage you to check out and try some of these recommendations.


<a id="org9d2814f"></a>

### Refactoring

-   [semantic-refactor](https://melpa.org/#/srefactor)

Great tool for refactoring base on semantic.


<a id="orgc1b6c29"></a>

### Cmake

-   [cmake-ide](https://github.com/atilaneves/cmake-ide)

If your project relies on cmake, this package may be what you are looking for, an integrated environment with a lot of utilities inside.


<a id="org2396bd8"></a>

### Emacs tree-sitter

-   [emacs-tree-sitter](https://github.com/ubolonton/emacs-tree-sitter)

This package provide great features for parsing and syntax highlight, it may be an interesting future package for core Emacs, it also have support for C and C++.


<a id="orga1a0ba4"></a>

## Extra features

These features are not bound to any C/C++ package per-se, but in my opinion they improve the overall programming experience.


<a id="org75f91de"></a>

### Hydra

-   [hydra](https://melpa.org/#/hydra)
-   [major-mode-hydra](https://melpa.org/#/major-mode-hydra)

A well know package from the mighty [abo-abo](https://github.com/abo-abo/), this configuration also uses `major-mode-hydra` a convenient addition to hydra.

```lisp

(major-mode-hydra-define (c++-mode c-mode) 
  (:title "C/C++ Mode" :color blue :separator "=" :exit t)
  ("Format/Indent"
   (("b" clang-format-buffer "buffer")
    ("r" c-indent-defun "region"))
   "Flycheck"
   (("e" flycheck-list-errors "errors"))
   "Navigate to"
   (("d" semantic-ia-fast-jump "definition")
    ("R" semantic-symref "references")
    ("t" helm-etags-select "tags")
    ("s" helm-semantic "semantic"))
   "Info"
   (("S" semantic-ia-show-doc "symbol")
    ("c" semantic-speedbar-analysis "context"))
   "Tags"
   (("g" fer/generate-tags "generate"))
   "Project"
   (("w" fer/run-project "run")
    ("C" fer/compile-project "compile")
    ("D" fer/debug-project "debug")
    ("l" fer/release-project "release"))))


```


<a id="org71f030d"></a>

### Skeletor

-   [skeletor](https://melpa.org/#/skeletor)

A package for project template, great addition to really transform Emacs into an IDE, really easy to use and useful.


<a id="orgc4771e1"></a>

## Alternatives

There are of course other alternatives out there for C and C++ development, for me these 2 are the most complete ones.


<a id="orgdf71783"></a>

### LSP

-   [ccls](https://melpa.org/#/ccls)

Language Server Protocol, one of the most interesting editor and IDEs technology, brings to emacs a great LSP server called ccls, and with the help of the lsp-mode team, we can have a great C/C++ experience OOTB.


<a id="orgd5dc9b9"></a>

### Rtags

-   [rtags](https://melpa.org/#/rtags)

Previous mentioned, it is an auto-complete, indexing and checker package that with the help of LLVM and clang it can perform great with medium/big projects.


<a id="orgd9d8e77"></a>

## Try it!

If you want to try this configuration in a plain emacs, this is a small .emacs.d to try the core features:

[Gitlab project](https://gitlab.com/sasanidas/c-try)

Clone this repo and move your .emacs.d, putting this in his place.


<a id="orgf8f1831"></a>

## Final thoughts

As show in this article, Emacs can become an awesome IDE for almost all language in existence, it only need a dedicated hacker (or group of) to invest some time. This article is inspire by [c-ide](https://tuhdo.github.io/c-ide.html) article, really recommended lecture if you want to know more in depth about some of these topics.

Bye and happy hacking!
