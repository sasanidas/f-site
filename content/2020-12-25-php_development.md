+++
  title = "PHP development in Emacs"
  description = "Article"
  +++

- [PHP development in Emacs](#orga5e88d1)
  - [Basic Elements](#orgb95e329)
    - [Major mode, formatting and indentation](#org9ba49fd)
    - [Auto-complete and function information](#org3f8d760)
    - [Error checking](#orgfb21824)
    - [Indexing and source code browsing](#org7d35f16)
  - [More features](#orgde8b2a5)
    - [PHPactor](#org426fd98)
    - [PHPunit](#org96b53d6)
  - [Extra features](#org839fe06)
    - [Debugging](#org3efb856)
    - [Psalm](#org854f9b7)
    - [Sonarlint](#orgf85acaf)
  - [Why?](#orgde049f5)


<a id="orga5e88d1"></a>

# PHP development in Emacs

PHP is an interesting language, been THE language in the backend, it's quite surprising the lack of tools that the language have, this is partially because of the great IDEs that attract a lot the developers , because is a quite boring language to hack on it and that it's mostly used in the enterprise world.

That doesn't mean that Emacs doesn't have any tool to it, but it is not "great" by default, you have to make some tweaks here and there to make it shine.

In the recent years there is also a new interesting technology in programming languages the [LSP](https://en.wikipedia.org/wiki/Language_Server_Protocol) that give Emacs hope in some languages that doesn't have too much traction in the Emacs ecosystem.

This article also assume that some tools are already installed in Emacs like [projectile](https://github.com/bbatsov/projectile) and [use-package](https://github.com/jwiegley/use-package).


<a id="orgb95e329"></a>

## Basic Elements


<a id="org9ba49fd"></a>

### Major mode, formatting and indentation

-   [php-mode](https://melpa.org/#/php-mode)
-   [phpcbf](https://melpa.org/#/phpcbf)

Let's start with setting some general variables, like the coding style and some executable locations.

```lisp

(defvar user/phpcbf-executable "/path/to/phpcbf.phar")
(defvar user/php-style "PSR12")


```

Then we can make use of [use-package](https://github.com/jwiegley/use-package) an Emacs "require" organiser, to install the main php-mode and some config.

```lisp

(use-package php-mode
  :ensure t
  :defer t
  :config (setq php-mode-coding-style "PSR-2"))

```

If we are using Drupal, we can change the last line to "Drupal" instead of PSR-2. Then, we are going to configure [phpcbf](https://github.com/squizlabs/PHP_CodeSniffer/wiki/Fixing-Errors-Automatically), a tool to property indent our code.

We need to make sure that the standard is available in the phpcbf, to check it just run `phpcbf -i` and it will show a list of the installed standards.

```lisp

(use-package phpcbf
  :defer t
  :ensure t
  :config 
  (setq phpcbf-executable user/phpcbf-executable
	phpcbf-standard user/php-standard))

```

To enable Drupal formatting, we have to change the value in `user/php-standard` and install the standard to phpcbf, [this page](https://www.drupal.org/node/1419988) is a nice guide to install it.

The Emacs command `phpcbf` will format our code according with the style defined.


<a id="org3f8d760"></a>

### Auto-complete and function information

-   [ac-php](https://github.com/xcwen/ac-php)
-   [company-mode](https://github.com/company-mode/company-mode)

Starting with autocompletion, we are going to set up [company-php](https://melpa.org/#/company-php) a subset of ac-php.

```lisp

(use-package company
  :ensure t
  :config (global-company-mode))


(use-package company-php
  :defer t
  :ensure t)


```

If we have already company configured, we can skip the first `use-package`. Now we have to create a hook, to set up properly completion inside a php-mode buffer.

```lisp
(add-hook 'php-mode-hook '(lambda ()
			    (ac-php-core-eldoc-setup)
			    (set (make-local-variable 'company-backends)
				 '(company-ac-php-backend company-capf company-dabbrev-code
							  company-files))))

```

This will also configure [ElDoc](https://www.emacswiki.org/emacs/ElDoc) which is the Emacs way to check function information.


<a id="orgfb21824"></a>

### Error checking

-   [flycheck](https://melpa.org/#/flycheck)

On fly error checking is almost mandatory for any programming environment, Emacs has a great tool for that task, with `flycheck`, it has the PHP checker OOTB but in order to give configure more advance checkers, like [phpcs](https://github.com/squizlabs/PHP_CodeSniffer), it is necessary to set the correct variables:

```lisp
(defvar user/phpcs-executable "/path/to/phpcs")

(use-package flycheck
  :ensure t
  :config
  (setq flycheck-php-phpcs-executable user/php/phpcs-executable
	flycheck-phpcs-standard	user/php-style))


```

We use again use-package to install the package and we set the correct phpcs executable location (the variable `user/php-style` was define in the major-mode section).

Now, if we call the function `flycheck` or set it [global](https://www.flycheck.org/en/latest/user/quickstart.html#enable-flycheck), we will have the PHP and the phpcs check.


<a id="org7d35f16"></a>

### Indexing and source code browsing

-   [universal-ctags](https://ctags.io/)

Searching for the reference/definition is a key feature, if we want to have such a feature, we will use universal-ctags, a simple tool that basically index all the project to a TAGS file that we can easily navigate with the Emacs interface. (Instruction to install the ctags tool are in the header of the section)

The tool can be invoke from the command line, but we are in Emacs, so we can easily create a simple function to create and update our TAG file.

```lisp

(defun user/generate-tags ()
  "Generate TAG file for PHP projects"
  (interactive)
  (let* ((project-root (projectile-project-root)))
    (shell-command
     (concat "cd " 
	     (shell-quote-argument project-root)  
	     " && ctags-universal -h \".php.inc.module\" -e -R " (shell-quote-argument project-root)))))

```

This function override the entire TAG files every time is invoke, for a more efficient approach, you can look up &#x2013;append option, but for small/medium project, I didn't find any performance issues.

Once we have our project indexed, we can use some useful `xref` features, a built in package.

| Function                                | Explanation                                  |
|--------------------------------------- |-------------------------------------------- |
| xref-find-references                    | Show references of the selected tag element  |
| xref-find-definitions                   | Show definitions of the selected tag element |
| (if helm is installed)helm-etags-select | Navigate through project tags                |

For more information about xref: [xref](https://www.gnu.org/software/emacs/manual/html_node/emacs/Xref.html)


<a id="orgde8b2a5"></a>

## More features


<a id="org426fd98"></a>

### PHPactor

-   [phpactor.el](https://github.com/emacs-php/phpactor.el)
-   [transient](https://github.com/magit/transient)

Describe as a "Completion and Refactoring tool for PHP", [Phpactor](https://phpactor.readthedocs.io/en/develop/) add great IDE capabilities to the table, with the Emacs phpactor package, it give us some interesting features, the configuration is quite easy, only requiring the phpactor executable location.

```lisp
(defvar user/phpactor-executable "/path/to/phpactor")

   (use-package phpactor
     :ensure t
     :after php-mode
     :config
     (setq phpactor-executable user/phpactor-executable))

```

It is recommended to check the [phpactor reference](https://phpactor.readthedocs.io/en/develop/reference.html), to check what feature we are interesting in, but overall all of the are interesting in our PHP development environment.

To have a more easy way to access all the commands, it is interesting to set up some interface. In the README of the phpactor.el repository we can extract the transient code.

```lisp


(use-package transient
  :ensure t)


(define-transient-command php-transient-menu ()
  "Php"
  [["Class"
    ("cc" "Copy" phpactor-copy-class)
    ("cn" "New" phpactor-create-new-class)
    ("cr" "Move" phpactor-move-class)
    ("ci" "Inflect" phpactor-inflect-class)
    ("n"  "Namespace" phpactor-fix-namespace)]
   ["Properties"
    ("a"  "Accessor" phpactor-generate-accessors)
    ("pc" "Constructor" phpactor-complete-constructor)
    ("pm" "Add missing props" phpactor-complete-properties)
    ("r" "Rename var locally" phpactor-rename-variable-local)
    ("R" "Rename var in file" phpactor-rename-variable-file)]
   ["Extract"
    ("ec" "constant" phpactor-extract-constant)
    ("ee" "expression" phpactor-extract-expression)
    ("em"  "method" phpactor-extract-method)]
   ["Methods"
    ("i" "Implement Contracts" phpactor-implement-contracts)
    ("m"  "Generate method" phpactor-generate-method)]
   ["Navigate"
    ("x" "List refs" phpactor-list-references)
    ("X" "Replace refs" phpactor-replace-references)
    ("."  "Goto def" phpactor-goto-definition)]
   ["Phpactor"
    ("s" "Status" phpactor-status)
    ("u" "Install" phpactor-install-or-update)]])

```

Calling the command `php-transient-menu`, give us a easy to navigate menu of all the capabilities of the phpactor.el package.


<a id="org96b53d6"></a>

### PHPunit

-   [phpunit](https://github.com/nlamirault/phpunit.el)

As developers, we all know how important the tests are for our software, in the case of PHP, the most well know test suit is [phpunit](https://phpunit.de/index.html), and Emacs has an easy package to interact with it.

```lisp

(use-package phpunit
  :ensure t
  :after php-mode
  :config
  (setq phpunit-configuration-file "phpunit.xml")
  (setq phpunit-root-directory (projectile-project-root)))

```

It is interesting to specify the default phpunit configuration file and the root directory of the project, using projectile, this is quite easy as showed.

All the available commands are describe [here](https://github.com/nlamirault/phpunit.el#available-commands), in the package README.


<a id="org839fe06"></a>

## Extra features


<a id="org3efb856"></a>

### Debugging

-   [dbgpClient](https://xdebug.org/docs/dbgpClient)
-   [realgud-xdebug](https://github.com/realgud/realgud-xdebug)

Recently, one of the maintainers of Xdebug, start building a CLI xdebug client, that can take care of all the xdebug protocol and give the developer a nice, clean interface.

The problem with this approach is that been a CLI program, it doesn't provide too much language tools so it's quite limited.

Fortunately, a nice guy (aka me), wrote a [realgud](https://github.com/realgud/realgud) extension that support xdebug with all the main features I usually use in my debugging with xdebug (more information in the project [README](https://github.com/realgud/realgud-xdebug/blob/master/README.org)).


<a id="org854f9b7"></a>

### Psalm

-   [psalm](https://psalm.dev/)

Psalm is an interesting tool, and it has some interesting features, there is a couple of ways it can be use, but it is recommended to read the [configuration](https://psalm.dev/docs/running_psalm/configuration/) first so all works well together.

There is a [MELPA psalm](https://github.com/emacs-php/psalm.el) project, but for me it was too much complicate for the thing I want to accomplish, psalm can do quite a loot of things, from [refactoring](https://psalm.dev/docs/manipulating_code/refactoring/) to [security analysis](https://psalm.dev/docs/security_analysis/), but I really only want some nice code checking.

So I wrote a small plugin for [flycheck-psalm](https://gitlab.com/sasanidas/flycheck-psalm/-/blob/master/flycheck-psalm.el): (I'm using [straight.el](https://github.com/raxod502/straight.el) to get the source code)

```lisp

(use-package flycheck-psalm
  :straight (flycheck-psalm :type git :host gitlab :repo "sasanidas/flycheck-psalm")
  :config
  (setq flycheck-psalm-executable "/path/to/psalmexecutable"))

```


<a id="orgf85acaf"></a>

### Sonarlint

-   [sonarlint](https://www.sonarlint.org/)
-   [lsp-sonarlint](https://github.com/emacs-lsp/lsp-sonarlint)

Sonarlint is a nice tool to have when I'm working with other people using OTHERS php environments. It unifies the tools and give us some nice checks.

And of course, even tho is not official, Emacs has a nice integration written by a handsome guy. (Second link in the section header).


<a id="orgde049f5"></a>

## Why?

So, one one of the question is: Why go writing all of this configuration when I can just use **INSERT PROPRIETARY ALTERNATIVE** and everything works?

And it is in fact, a great question, but it also misses the point. If you really only want OOTB experience and don't care about anything else, yes, go ahead, stop reading here. BUT if you think that development in PHP doesn't deserve the lock up of tools that in some areas have,then I think learning how to use the variety of free software tools available may help you understand that, the proprietary software may have the advantage right now, but I think in the future the PHP development maybe more feature rich for free software developers.
