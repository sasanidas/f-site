+++
    title = "LSP is the troyan horse of Microsoft"
    description = "Article"
    +++

- [LSP is the troyan horse of Microsoft](#org7ce4e66)
  - [Github](#org9a6e49e)
  - [VScode](#org66e409b)
  - [LSP](#orgb08cd45)
  - [LSP problems](#org76c305d)
  - [LSP BIG problem](#org77e9032)
  - [Conclusion](#org0f44385)


<a id="org7ce4e66"></a>

# LSP is the troyan horse of Microsoft

The LSP protocol is an "nice" way of decouple the "language engine"(libraries that take care of things related to indexing, autocompletion, tooltip&#x2026;) and the editor, creating a common backend.

This gives the editors the ability to just implement a client, and add support for a variety of languages, reducing the development time of implementing new language support.

All and all, this sound amazing, it's like a "plug and play" but for programming languages , it can save development time and get the editor closer to a full IDE.

**Where is the catch?**


<a id="org9a6e49e"></a>

## Github

We are in the year 2018, Github is the most used git [forge](https://en.wikipedia.org/wiki/Forge_(software)) in the world, it contains probably 80%~ of new free software projects, and even tho the backend is proprietary, the web interface is nice and encourage a platform for central collaboration so is easy to find people and projects.

Github is also developing an editor (Atom) based on the [electron](https://www.electronjs.org/) framework, which uses the chromium browser behind so it has all the capabilities of a browser, and been that is written in javascript, there is more developers available to write extensions.

So, Github is basically a [single point of failure](https://en.wikipedia.org/wiki/Single_point_of_failure) for the free and open source community that Microsoft smartly exploit, [buying it](https://en.wikipedia.org/wiki/GitHub#Acquisition_by_Microsoft) for US$7.5 billions.

This is probably one of the biggest tragedies in the free software history, along with the [acquisition of Sun](https://en.wikipedia.org/wiki/Sun_Microsystems#Acquisition_by_Oracle) by Oracle, Microsoft played really smart, making some of his software "open source" to convinced some part of the FLOSS community that they are now "the good guys" this [has been refute](https://www.gnu.org/proprietary/malware-microsoft.html) countless of times, but the marketing team of M$ can really make people think otherwise.


<a id="org66e409b"></a>

## VScode

VScode is a general purpose code editor, it is also base on the electro framework, but in order **not** to copy 100% Atom (at the time, 2015 Github wasn't a property of M$), they manage to create LSP (along with Read Hat) so the new editor can be build around this new LSP concept.

Still, Atom was doing pretty good, so the Github acquisition was almost mandatory to ensure that there is only **one** go to editor for web related languages.

As a side note, the VScode that 90% of the people download [is proprietary](https://code.visualstudio.com/license).


<a id="orgb08cd45"></a>

## LSP

So, given this context, we can see why VScode is right now the most editor used in the world:

-   It has a major company behind
-   It has virtually no competition (at least in web related technology)

Giving that LSP is an open protocol, that is supported by virtually all the major editors, it is not crazy to think that editor will move to this concept of "just been a front-end for LSP", so every new editor can focus only in the "non language integration part".


<a id="org76c305d"></a>

## LSP problems

-   Not all LSP language servers provide the same functionality
    
    This means that the language you chose to develop, may have different support depend on the LSP that you are using, some of them will have excellent support, while others may provide just a couple of basic things.

-   Understanding that VScode is written in typescript (a language controlled by M$), there is a lot of language written in that language.
    
    This means that in order to get code completion support for Dockerfile ([for example](https://github.com/rcjsuen/dockerfile-language-server-nodejs)) you need to have Nodejs, which if you only use VScode, no problem, right?

-   LSP complexity to a rather easy to solve problem.
    
    Some people just want some code completion and goto definition, having a server running in the background with hundreds of calls it's a huge overkill.


<a id="org77e9032"></a>

## LSP BIG problem

Microsoft is using this strategy to attract user to this paradigm, and then [block](https://github.com/microsoft/vscode-cpptools/issues/2679) the extensions they want to, sometimes [deprecating a free software project in favor of a proprietary one](https://github.com/microsoft/python-language-server/issues/2096#issuecomment-663805032), this is the big deal.

Remember, this is the standard M$ strategy, they move the competition, making his software the only viable option, and then make the stack proprietary so all the market is controlled by them, this is the well know strategy of [embrace, extend, and extinguish](https://en.wikipedia.org/wiki/Embrace,_extend,_and_extinguish).


<a id="org0f44385"></a>

## Conclusion

Don't trust M$, use [Emacs](https://www.gnu.org/software/emacs/) and embrace GNU.
